![logo gitlab](https://upload.wikimedia.org/wikipedia/commons/e/e1/GitLab_logo.svg)
# <span style="color: orange;background-color: black;text-stroke: 20px white">GitLab Cheat Sheet</span>
---
##00 Git configuration
`$ git config --global user.name “Your Name”`
<small>Set the name that will be attached to your commits and tags.</small>
`$ git config --global user.email “you@example.com”`
<small>Set the e-mail address that will be attached to your commits and tags.</small>
`$ git config --global color.ui auto`
<small>Enable some colorization of Git output.</small>

---

##01 Starting A Project
`git init [project name]`
<small>Create a new local repository. If [project name] is provided, Git will create a new directory name [project name] and will initialize a repository inside it. If [project name] is not provided, then a new repository is initialized in the current directory.</small>

`$ git clone [project url]` 
<small>Downloads a project with the entire history from the remote repository.</small> 
<small>if you use token you can replace <u>[project url]</u>=> https://[user]:[token]@[gitlab.example.com/project]</small>

---

##02 Git branching model

`$ git branch [-a]`
<small>List all local branches in repository. With -a: show all branches(with remote).</small>

`$ git branch [branch_name]`
<small>Create new branch, referencing the current HEAD.</small>

`$ git checkout [-b] [branch_name]`
<small>Switch working directory to the specified branch. With -b: Git will create the specified branch if it does not exist.</small>

`$ git checkout [--track] [branch_name]`
<small>switch to a different branch in a Git repository by specifying the name of the branch to use instead of <u>[branch_name]</u>.</small>

`$ git merge [from name]`
<small>Join specified <u>[from name]</u> branch into your current branch (the one you are on currently).</small>

`$ git branch -d [name]`
<small>Remove selected branch, if it is already merged into any other. -D instead of -d forces deletion.</small>

---

##03 To-Work
`$ git status`
<small>Displays the status of your working directory. Options include new,staged, and modified files. It will retrieve branch name, current commit identifier, and changes pending commit.</small>

`$ git add [file]`
<small>Add a file to the staging area. Use in place of the full file path to add all changed files from the current directory down into the directory tree.</small>

`$ git diff [file]`
<small>Show changes between working directory and staging area.</small>

`$ git commit [-m] "[ message ]"`
<small>Create a new commit from changes added to the staging area.The commit must have a message!</small>

---

##04 Review your work

`$ git log [-n count]`
<small>List commit sha history of current branch. -n count limits list to last n commits.</small>

`$ git log --oneline --graph --decorate`
<small>An overview with reference labels and history graph. One commit per line.</small>

`$ git reflog`
<small>List operations (e.g. checkouts or commits) made on local repository</small>

---

##05 Tagging known commits

`$ git tag`
<small>List all tags.</small>

`$ git tag [name] [commit sha]`
<small>Create a tag reference named name for current commit. Add commit sha to tag a specific commit instead of current one.</small>

`git tag -a [tagname] [-m] "[Tag message]"`
<small>Create a tag with message detail</small>

`git show [tagname]`
<small>Show detail tag</small>

`git push [remote repository] [tagname]`
<small>push tag to remote repository</small>

`git push [--tags]]`
<small>push all tags to remote repository</small>

`git push [remote_name] --delete [tag_name]`
<small>remove a tag from a remote repository</small>

`$ git tag -d [name]`
<small>Remove a tag from local repository tag test</small>

---

##06 Reverting changes

`git reset [commit]`
<small>Revert back to a previous commit without deleting related commits:</small>

`git reset --abort`
<small>Cancel the previous reset</small>

`git reset [file]`
<small>Remove a file from the staging area:</small>

`git reset`
<small>Remove all files from the staging area</small>

`git reset --hard <commit>`
<small>Switches the current branch to the target reference leaving a difference as an uncommitted change. When --hard is used, all changes are discarded.</small>
><small><span style="padding: 1px; font-weight: 50;color: black;">
 <span style="color: orange"><u>In soft mode</u></span>, we revert back to the previous commit without removing any files from the staging area, <span style="color: orange"><u>In hard mode</u></span>, we delete the files from the staging area and revert back to the previous commit entirely.</span></small>


`$ git revert [commit sha]`
<small>Create a new commit, reverting changes from the specified commit. It generates an inversion of changes.</small>

`git revert --abort`
<small>Cancel the previous revert</small>
><small><span style="padding: 1px; font-weight: 50;color: black;">
In general, if changes have not been committed yet, it is better to use <span style="color: orange;"><u>reset</u></span> to remove them, while if changes have already been committed and pushed to the remote repository, it is better to use <span style="color: orange;"><u>revert</u></span> to avoid losing any data or changes made by other contributors.</span></small>

---

##07 Synchronizing repositories

`$ git fetch [remote]`
<small>Fetch changes from the remote, but <span style="color: orange;"><u>not update</u></span> tracking branches</small>

`$ git fetch --prune [remote]`
<small>Delete remote Refs that were removed from the remote repository.For example, if a branch named featureA was deleted from the remote repository after it was completed, but it still exists in the local repository, using git fetch --prune will remove the reference of featureA from the local repository.</small>

`$ git pull [remote]`
<small>Fetch changes from the remote and <span style="color: orange;"><u>merge</u></span>  current branch with its upstream.</small>

`$ git push [--tags] [remote]`
<small>Push local changes to the remote. Use --tags to push tags.</small>

`$ git push -u [remote] [branch]`
<small>Push local branch to remote repository. Set its copy as an upstream.</small>

---
